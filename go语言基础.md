# happyLearningGolang <br>
****golang变量表达式****<br>
    var+变量名字类型 = 表达式<br>
多种变量表达<br>
1.常规<br>
① var i,j,k int<br>
② var b,f,s = true ,2.3, "four"<br>
2.简短变量声明<br>
    i,j := 0, 1<br>
<br>
**实例**<br>
i := 100                          //an int<br>
var boiling float64 = 100         //an float64<br>
var names []string<br>
var err error<br>
var p point<br>
<br>
打开/关闭文件<br>
f , err := os.open(infile)<br>
f , err := os.Create(outfile)<br>

<br>
<br>
****数组(array)****<br>
array的定义<br>
定义数组的格式：var <varName>[n]<type>,n>=0    /*[n]<type>表示长度，长度也是数组类型的一部分*/<br>
                =>var a  [2]int / a := [2]int{ 1,1 }<br>
    索引法=>  a := [20]int{19:1}   (第20个元素为1)<br>
    不指定数组长度=> a := [...]int{1,2,3,4,5} <br>
指向数组的指针和指针数组<br>
    指向数组的指针：var p* [100]int = &a<br>
    指针数组： x,y := 1,2   a := [...]* int{&x,&y}<br>
    p := new([10]int)<br>
    println=>&[0 0 0 0 0 0 0 0 0 ]<br>
多维数组<br>
    a := [2][3]int{<br>
        {1,1,1}<br>
        {2,2,2}<br>
    }<br>
<br>
**数组的应用**<br>
golang版冒泡排序：<br>
<br>
func main(){<br>
    a := [...]int{5,2,6,3,9}<br>
    fmt.Println(a)<br>
    num := len(a)<br>
    for i := 0; i<num; i++{<br>
        for j := i+1; j<num; j++{<br>
            if a[i] < a[j]{<br>
                temp := a[i]<br>
                a[i] = a[j]<br>
                a[j] = temp<br>
            }<br>
        }<br>
    }<br>
    fmt.Println(a)<br>
}<br>
<br>
****切片(slice)****<br>
声明： var s1[]int<br>
slice切片操作s[i:j],其中0<=i<=j<=cap(s)<br>
用于创建一个新的slice，引入s从第i个元素开始到第j - 1个元素的子序列<br>
_注：slice不能用 == 进行比较，用bytes.Equal来判断两个slice是否相等_<br>
创建slice<br>
s1 := make([]int,3,10)    /*3表示slice长度len，10表示slice的容量cap*/<br>
_注：创建的slice如果超过容量，则增加原有容量一倍的连续内存地址_<br>
<br>
Reslice<br>
    ①Reslice时索引以被slice的切片为准<br>
    例：<br>
        s1 := make([]int,8,10)<br>
        s1 = {0,1,2,3,4,5,6,7}<br>
        s2 := s1[3:6]<br>
        Println(s2)<br>
            =>[2 3 4]     /*此时s2的长度len=3，容量cap=8*/<br>
    ②索引不可以超过被slice的切片容量cap()的值<br>
    ③索引越界不会导致底层数组的重新分配，而是引发错误<br>
<br>
Append(用Append追加元素不会影响底层数组)<br>
    ①可以在slice尾部追加元素<br>
        例：<br>
            s1 := []int{1,2,3}<br>
            s1 = append(s1,1,2,3,4,5)<br>
            =>[1,2,3,1,2,3,4,5]<br>
    ②可以将一个slice追加在另一个slice的尾部<br>
        s3 := append(s1,s2)<br>
    ③如果最终长度未超过追加到的slice的容量则返回原始slice<br>
    ④如果超过追加到的slice的容量则重新分配数组并拷贝到原始数组<br>
<br>
copy<br>
    用法：<br>
        s1 := []int {1,2,3,4,5,6}<br>
        s2 := []int {7,8,9}<br>
        copy(s1,s2)<br>
            =>[7 8 9 4 5 6]           /*把s2赋值到s1前面的n个元素，n = len(s2)*/<br>
        copy(s2,s1)<br>
            =>[1 2 3]                 /*把s1前面的n个元素赋值到s2，n = len(s2)*/<br>
        copy(s1[3:5],s2[2:4])<br>
            =>[1 2 8 9 5 6]           /*指定索引赋值*/<br>
<br>
⭐使用slice使其中一个指针指向底层数组发生变化，则底层数组也会发生变化<br>
    例子：<br>
        a := []int{1,2,3,4,5}<br>
        s1 := a[2:5]<br>
        s2 := a[1:3]<br>
        fmt.Println(s1,s2)<br>
            /*=>[3 4 5][2 3]*/<br>
        s1[0] = 9<br>
        fmt.Println(s1,s2)<br>
            /*=>[9 4 5][2 9]*/<br>
<br>
slice之测试相同月份<br>
    func main(){<br>
        month := [...]string{1:"january",2:"February",3:"March",4:"April",5:"May",6:"june",7:"july",8:"August",9:"September",10:"October",11:"November",12:"December"}<br>
        Q2 := month[4:7]<br>
        summer := month[6:9]<br>
            for _ , s:range summer{<br>
                for _ ,q := range Q2{<br>
                    if s == q{<br>
                        fmt.Println("%s appears in both",s)<br>
                    }<br>
                }<br>
            }<br>
    }<br>
<br>
<br>
****map****<br>
    以key-value 形式存储数据<br>
    map查找比线性搜索快很多，但比使用索引访问数据类型慢100倍<br>
    map使用make()创建，支持 := 这种简易写法<br>
    =>make([key type] value type , cap)  cap表示容量，可以省略<br>
        m := make(map[int] string)<br>
    删除对应键的值<br>
        delete(m,1)  //删除key为1的value<br>
    map类型的slice<br>
        sm := make([]map[int] string , 5)<br>
    对无序的map进行有序排序<br>
        package main<br>
        import(<br>
            "fmt"<br>
            "sort"<br>
        )<br>
        func main(){<br>
            m := make([int]string){1:"a",2:"b",3:"c",4:"d",5:"d"}<br>
            s := make([string]int , len(m))<br>
            i := 0<br>
            for k , _:=range m{<br>
                s[i] = k<br>
                i++<br>
            }<br>
            sort.Ints(s)<br>
            fmt.Println(s)<br>
        }<br>
<br>
****函数function****<br>
    函数声明包括函数名，形式参数列表，返回值列表以及函数体<br>
    格式：<br>
        func name(parameter-list)(result-list){<br>
            body<br>
        }<br>
    以下两个声明是等价的<br>
    func f( i,j,k int , s,t string )<br>
    func f( i int, j int, k int, s string, t string)<br>
<br>
    匿名函数：只执行一次就会被回收，当某一个功能只需要执行一次就应该被定义为匿名函数<br>
<br>
    **递归**<br>
        例：<br>
            func Recursive(n int) int{<br>
                if n == 0{<br>
                    return 1<br>
                }<br>
                return Recursive(n-1)<br>
            }<br>
            func main(){<br>
                fmt.Println(Recursive(10))<br>
            }<br>
<br>
